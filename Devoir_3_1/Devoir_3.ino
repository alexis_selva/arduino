// Devoir 3 : 

#define   P1              2
#define   P2              3
#define   L1              4
#define   L2              5
#define   PERIOD_PUSH     200
#define   PERIOD_BETWEEN  500

void led_on(byte led) {
  PORTD |= (1 << led);
}

void led_off(byte led) {
  PORTD &= ~(1 << led);
}

byte push_on(byte push) {
  return PIND & (1 << push);
}

void evaluate_count(int count_p1, int count_p2) {
  if (count_p1 < count_p2) {
    led_on(L1);
    led_off(L2);
  } else if (count_p1 > count_p2) {
    led_off(L1);
    led_on(L2);
  } else {
    led_off(L1);
    led_off(L2);
  }
}

void setup () {
  PIND = 0;
  PORTD = 0;
  DDRD |= (1 << L1) | (1 << L2);
}

int count_p1 = 0;
int count_p2 = 0;

void loop () {
  if (push_on(P1)) {
    while (!push_on(P1)) { }
    delay(PERIOD_PUSH);
    count_p1++;
  }
  if (push_on(P2)) {
    while (!push_on(P2)) { }
    delay(PERIOD_PUSH);
    count_p2++;
  }
  evaluate_count(count_p1, count_p2);
  delay(PERIOD_BETWEEN);
}
