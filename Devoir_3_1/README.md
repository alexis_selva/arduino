Devoir de programmation semaine 3

Statistique des poussoirs

On presse alternativement et irrégulièrement sur le poussoir P1 et sur le poussoir P2.

Le programme doit allumer L1 si on a pressé plus souvent sur P1, et L2 si c'est le contraire.
Le correcteur enverra des "pression" sur P1 et P2 d'une durée d'environ 0.2s, séparées par une demi-seconde.
