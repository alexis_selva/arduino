Devoir de programmation semaine 7

Robot peureux

Dans cet exercice, on se limitera à la commande d'un seul moteur, à partir de la lecture d'un capteur de lumière. Le comportement du robot sera le suivant : 
- en absence d'une lumière importante, le robot va avancer très lentement (il est curieux, malgré tout...) 
- dès qu'un détecte une lumière, il va reculer. Sa vitesse de recul sera d'autant plus grande que la lumière est forte (prédateur proche...)

Voici des explications pour la réalisation : 
- avec le matériel du kit Diduino : RobotPeureuxArduino.pdf 
- avec le matériel du kit MSP430 : RobotPeureuxMsp.pdf

Voici les consignes : 
- le "point de basculement" du comportement du robot sera le point milieu de l'alimentation 
- le moteur : L1 pour l'avance, L2 pour le recul. 
- le capteur avant : A1 pour l'Arduino, P1_7 pour le MSP430
