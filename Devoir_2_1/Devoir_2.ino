// Devoir 2 : 

#define   L1            4
#define   L2            5
#define   LED_1_ON      digitalWrite (L1,1)
#define   LED_1_OFF     digitalWrite (L1,0)
#define   LED_2_ON      digitalWrite (L2,1)
#define   LED_2_OFF     digitalWrite (L2,0)
#define   PERIODE       1000
#define   WAIT          delay(PERIODE)
#define   P1            2
#define   P2            3
#define   POUSSOIR_1_ON (digitalRead (P1) == 0)
#define   POUSSOIR_2_ON (digitalRead (P2) == 0)

void setup() {
  pinMode (P1, INPUT);
  pinMode (P2, INPUT);
  pinMode (L1, OUTPUT);
  pinMode (L2, OUTPUT);
}

void loop () {
  while (POUSSOIR_1_ON) {}
  if (!POUSSOIR_1_ON) {
    LED_1_ON;
    WAIT;
    LED_2_ON;
    WAIT;
    LED_1_OFF;
    WAIT;
    LED_2_OFF;
    WAIT;
  }
}

