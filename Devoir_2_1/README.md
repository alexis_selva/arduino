Devoir de programmation semaine 2

Un premier devoir très simple vous est proposé pour vous mettre en confiance :

Petite séquence L1-L2

Lorsqu’on presse sur le Poussoir P1, la LED L1 doit s'allumer durant une seconde. Ensuite la LED L2 doit aussi s'allumer (les deux seront allumées) durant une seconde. Finalement, seule L2 sera allumée, également une seconde. Après, les 2 LED seront éteintes et le programme attendra de nouveau qu'on presse sur le Poussoir.

