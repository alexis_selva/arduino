Voici les devoirs de programmation concernant les microcontrôleurs:

* Devoir 1 : 1er exemple
* Devoir 2 - 1 : jouer une petite séquence L1-L2
* Devoir 2 - 2 : feu à l'entrée d'un parking
* Devoir 3 - 1 : statistique des poussoirs
* Devoir 3 - 2 : SOS du Titanic !
* Devoir 4 - 1 : commande d'un moteur pas-à-pas
* Devoir 4 - 2 : serrure codée
* Devoir 5 - 1 : lecture analogique
* Devoir 5 - 2 : ligne série
* Devoir 6 - 1 : détection du rapport cyclique d'un signal rectangulaire
* Devoir 6 - 2 : remplissage d'une bouteille avec apprentissage du temps
* Devoir 7 - 1 : commande d'un robot mobile (éviter les obstacles)
* Devoir 7 - 2 : robot peureux

Pour plus d'information, je vous invite à jeter un coup d'oeil à l'adresse https://www.coursera.org/course/microcontroleurs