Devoir de programmation semaine 5

Lecture analogique

Votre programme doit lire une valeur analogique et afficher dans quel intervalle elle se trouve par rapport à la tension d'alimentation :
- entre 0 et 25 % : L1 et L2 doivent être éteintes
- entre 25 et 50 % : seule L1 doit être allumée 
- entre 50 et 75 % : seule L2 doit être allumée
- entre 75 et 100 % : L1 et L2 doivent être allumées

Sur l'Arduino, utilisez la patte A1 comme entrée analogique. Sur le Launchpad, utilisez P1_7.
Voici des schémas de montage pour tester le programme et une photo avec un Launchpad :
