// Devoir 5 : alimentation 

#define   L1                    4
#define   L2                    5

static void led_on(byte led) {
  PORTD |= 1 << led;
}

static void led_off(byte led) {
  PORTD &= ~(1 << led);
}

static void change_leds(byte value) {
  if (value < 25) {
    led_off(L1);
    led_off(L2);
  } else if ((value >= 25) && (value < 50)) {
    led_on(L1);
    led_off(L2);
  } else if ((value >= 50) && (value < 75)) {
    led_off(L1);
    led_on(L2);
  } else {
    led_on(L1);
    led_on(L2);
  }
}

void setup () { 
  PIND = 0;
  PORTD = 0;
  DDRD |= (1 << L1) | (1 << L2);
  Serial.begin(9600);
}
 
void loop () {
  int voltage = analogRead(A1);
  Serial.println (voltage);
  byte value = map (voltage, 0, 1023, 0, 100);
  change_leds(value);
  delay(1000);
}
