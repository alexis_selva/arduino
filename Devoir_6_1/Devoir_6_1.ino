// Devoir 6_1 : detection 

#define   P1              2
#define   L1              4
#define   L2              5
#define   LIMIT_1         40
#define   LIMIT_2         60
#define   BASE_TIME       10

enum {
  HIGH_LEVEL = 0,
  PERIOD,
  MAX_LEVELS,
};

static unsigned int delays[MAX_LEVELS] = { 0, 0 };

void led_on(byte led) {
  PORTD |= 1 << led;
}

void led_off(byte led) {
  PORTD &= ~(1 << led);
}

byte push_on(byte push) {
  return !(PIND & (1 << push));
}

ISR(TIMER2_OVF_vect) {
  static volatile unsigned int cnt = 0;
  static volatile byte old_level = push_on(P1);
  static volatile unsigned int current_delays[MAX_LEVELS] = { 0, 0 };
  TCNT2 = 56;
  if (cnt++ > BASE_TIME) {
    cnt = 0;
    byte level = push_on(P1);
    if (level) {
      current_delays[HIGH_LEVEL]++;
    }
    current_delays[PERIOD]++;
    if ((old_level) && (!level)) {
      delays[HIGH_LEVEL] = current_delays[HIGH_LEVEL];
      current_delays[HIGH_LEVEL] = 0;
    } else if ((!old_level) && (level)) {
      delays[PERIOD] = current_delays[PERIOD];
      current_delays[PERIOD] = 0;
    }
    old_level = level;
  } 
}

void evaluate_periods(void) {
  if (delays[PERIOD] > 0) {
    unsigned int rate = (100 * delays[HIGH_LEVEL]) / delays[PERIOD];
    if (rate < LIMIT_1) {
      led_off(L1);
      led_on(L2);
    } else if (rate > LIMIT_2) {
      led_on(L1);
      led_off(L2);
    } else {
      led_off(L1);
      led_off(L2);
    }
  } else {
    led_off(L1);
    led_off(L2);
  }
}
 
void setup () { 
  PORTD = 0;
  PIND |= (1 << P1);
  DDRD |= (1 << L1) | (1 << L2);
  TCCR2A = 0;
  TCCR2B = 0b00000010;
  TIMSK2 = 0b00000001;
  sei();
}

void loop () {
  evaluate_periods();
  
}
