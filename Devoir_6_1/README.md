Devoir de programmation semaine 6

Détection du rapport cyclique d'un signal rectangulaire

Un signal digital périodique est appelé carré si le temps pendant lequel le signal est à 1 est égal au temps pendant lequel il est à 0. Si ce n'est pas le cas, on parle souvent de signal rectangulaire. Le rapport cyclique est le temps pendant lequel le signal est à 1 divisé par la période.

On cherche à réaliser un dispositif avec une entrée et deux sorties. L'entrée reçoit un signal rectangulaire, dont la fréquence est de 1 à 100 Hz. La première sortie doit s'activer si le rapport cyclique est inférieur à 40%, la seconde s'il est supérieur à 60%. S'il n'y a pas de signal, ou si le signal est carré, aucune sortie ne doit s'activer.

Comme d'habitude, utilisez P1 comme entrée, L1 et L2 comme sorties.
