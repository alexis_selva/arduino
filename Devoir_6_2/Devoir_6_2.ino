// Devoir 6_2 : remplissage 

#define   P1              2
#define   P2              3
#define   L1              4
#define   L2              5
#define   LIMIT_1         40
#define   LIMIT_2         60
#define   BASE_TIME       10

static unsigned int delay_P1 = 0;

void led_on(byte led) {
  PORTD |= 1 << led;
}

void led_off(byte led) {
  PORTD &= ~(1 << led);
}

byte push_on(byte push) {
  return !(PIND & (1 << push));
}

ISR(TIMER2_OVF_vect) {
  static volatile unsigned int cnt = 0;
  static volatile byte old_level = push_on(P1);
  static volatile unsigned int current_delay_P1 = 0;
  TCNT2 = 56;
  if (cnt++ > BASE_TIME) {
    cnt = 0;
    byte level = push_on(P1);
    if (level) {
      led_on(L1);
      current_delay_P1++;
    }
    if ((old_level) && (!level)) {
      led_off(L1);
      delay_P1 = current_delay_P1;
      current_delay_P1 = 0;
    } 
    old_level = level;
  } 
}

void fill_bottle_auto(void) {
 if (delay_P1) {
   led_on(L1);
   delay(delay_P1);
   led_off(L1);
 }
}
 
void setup () { 
  PORTD = 0;
  PIND |= (1 << P1) | (1 << P2);
  DDRD |= (1 << L1) | (1 << L2);
  TCCR2A = 0;
  TCCR2B = 0b00000010;
  TIMSK2 = 0b00000001;
  sei();
}

void loop () {
  static byte old_level = push_on(P2);
  byte level = push_on(P2);
  if ((old_level) && (!level)) {
    fill_bottle_auto();
  } 
  old_level = level;  
}
