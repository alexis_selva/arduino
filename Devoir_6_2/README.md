Devoir de programmation semaine 6

Remplissage d'une bouteille avec apprentissage du temps

Dans un atelier artisanal de production de boissons, on cherche à accélérer la production. Deux boutons vont permettre de remplir une bouteille : 
- P1 doit être pressé pour activer la vanne de remplissage et maintenu pressé jusqu'à ce que la bouteille soit pleine. Le système apprends alors le temps de remplissage adapté à ce type de bouteilles. 
- P2 peut ensuite être simplement pressé et relâché : la bouteille va se remplir pendant la durée apprise.

L1 est utilisé pour la commande de la vanne de remplissage.
