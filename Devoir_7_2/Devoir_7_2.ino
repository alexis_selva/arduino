/* ASSIGNEMENT 7.2 */

/* DEFINITIONS */

#define CAPTEUR A1
#define LED_1  4
#define LED_2  5
#define MOTEUR_AVANCE  LED_1
#define MOTEUR_RECULE  LED_2

#define MIN_INTENSITE 0
#define MAX_INTENSITE 1023

#define MAX_PFM 16
#define COMPTEUR_PFM 20
#define COMPTEUR_LUMIERE 500

#define VITESSE_AVANCE 4

#define TIMER_CORRECTION 12

/* VARIABLES */

static char vit;
static volatile char pfm;
static volatile byte vstable;
static volatile char recule;

/* FUNCTIONS */

static inline void moteur_avance(void) {
  bitClear (PORTD, MOTEUR_RECULE); 
  bitSet   (PORTD, MOTEUR_AVANCE);
}

static inline void moteur_recule(void) {
  bitSet   (PORTD, MOTEUR_RECULE); 
  bitClear (PORTD, MOTEUR_AVANCE);
}

static inline void moteur_stoppe(void) {
  bitClear (PORTD,MOTEUR_RECULE); 
  bitClear (PORTD,MOTEUR_AVANCE);
}

static inline void setup_in_out (void) { 
  PIND = 0;
  PORTD = 0;
  DDRD = 0;
  bitSet (DDRD, MOTEUR_RECULE); 
  bitSet (DDRD, MOTEUR_AVANCE);
}

static inline void do_pfm (char pf) {
  static volatile byte pfmCnt;
  
  if (pf >= 0) {
    if (pf > MAX_PFM) {
      pf = MAX_PFM;
    }
    pfmCnt += pf;
    if (pfmCnt > MAX_PFM - 1) {
      pfmCnt &= (MAX_PFM - 1); 
      moteur_avance();
    } else { 
      moteur_stoppe(); 
    }
  } else {
    pf = -pf;
    if (pf > MAX_PFM) {
      pf = MAX_PFM;
    } 
    pfmCnt += pf;
    if (pfmCnt > MAX_PFM - 1) {
      pfmCnt &= (MAX_PFM - 1); 
      moteur_recule();
    } else { 
      moteur_stoppe();
    }
  }
}

static inline void set_vitesse (char v) {  
  if (v > pfm) {
    pfm++;
    vstable = 0;
  }
  if (v < pfm) {
    pfm--; 
    vstable = 0;
  }
  if (vit == pfm) {
    vstable = 1;
  }
}

static inline void next_vitesse (char v) {
  vit = v;
  delay (10);
  while (!vstable) {}
  delayMicroseconds (10);
}

static inline void mesure_lumiere (void)  {
  recule = map (analogRead(CAPTEUR), MIN_INTENSITE, MAX_INTENSITE, -MAX_PFM, MAX_PFM);
}

ISR (TIMER2_OVF_vect) {
  static volatile byte cnt1;
  static volatile unsigned int cnt2;
  
  /* Rearm the timer every 100us (200 * 8 * 62.5us)
     considering a correction of few us */
  TCNT2 = 256 - 200 + TIMER_CORRECTION;
  
  if (cnt1++ > COMPTEUR_PFM) {
    cnt1 = 0; 
    if (vit == pfm) {
      vstable = 1;
    } else {
      vstable = 0;
    }
    do_pfm (pfm);
  }
  
  if (cnt2++ > COMPTEUR_LUMIERE) {
    cnt2 = 0; 
    set_vitesse (vit);
    mesure_lumiere();
  }
}

/* Setup the interruption for the TIMER2 */
static inline void setup_timer(void) {
  cli();
  TCCR2A = 0;
  TCCR2B = 0b00000010; 
  TIMSK2 = 0b00000001;
  sei();
}

void setup()  { 
  setup_in_out();  
  setup_timer();
  next_vitesse (VITESSE_AVANCE);
}

void loop () {
  if (recule > 0) {
    next_vitesse (VITESSE_AVANCE);
  } else {
    next_vitesse (recule);
  }
}




