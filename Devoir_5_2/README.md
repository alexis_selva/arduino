Devoir de programmation semaine 5

Ligne série

Encore un SOS ! Mais cette fois, c'est sur un ligne série asynchrone que vous devez envoyer les trois caractères, en code ASCII. La sortie à utiliser est L1, pour vous permettre de contrôler votre programme à faible vitesse, avant de le régler à une vitesse de 300 bauds (durée d'un bit 3.33 ms). Attendez environ une seconde entre chaque SOS.

Bien entendu, n'utilisez pas la ligne série de l'Arduino (qui ne fournit pas ses signaux sur la patte L1) !
Le code hexadécimal de "S" est 0x53, celui de "O" est 0x4F, mais vous n'avez pas à l'indiquer dans votre programme : le compilateur les connaît ! N'oubliez pas que le bit de poids faible est envoyé le premier.
