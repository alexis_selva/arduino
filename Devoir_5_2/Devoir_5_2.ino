// Devoir 5_2 : sos 

#define   L1                    4
#define   L2                    5
#define   PERIOD_BIT            3000
#define   PERIOD_SOS            1000

/*
static volatile byte next = 0;
static int cnt = 0;
*/

void led_on(byte led) {
  PORTD |= 1 << led;
}

void led_off(byte led) {
  PORTD &= ~(1 << led);
}

void print_bit(byte value){
  /*
  while (next == 0) { };
  next = 0;
  */
  //led_on(L2);
  if (value) {
    led_off(L1);
  } else {     
    led_on(L1);
  }
  delayMicroseconds(PERIOD_BIT);
}

void print_character(byte value) {
  print_bit(1);
  for (byte i = 0 ; i < 8 ; i++) {
    print_bit(value & 1);
    value >>= 1;
  }
  print_bit(0);
  print_bit(0);
}

void print_sos(void){
  print_character('S');
  print_character('O');
  print_character('S');
  //led_off(L1);
  //led_off(L2);
}

void stop_sos(void) {
  /*
  cli();
  TCCR2B = 0;
  TIMSK2 = 0;
  sei();
  */
  led_on(L1);
  delay(PERIOD_SOS);
  led_off(L1);
  /*
  cli();
  TCCR2B = 0b00000010;
  TIMSK2 = 0b00000001;
  cnt = 0;
  next = 0;
  sei();
  */
}

/*
ISR(TIMER2_OVF_vect) {
  TCNT2 = 62;
  if (cnt++ > PERIOD_BIT) {
    cnt = 0;
    next = 1;
  }
}
*/
 
void setup () { 
  PIND = 0;
  PORTD = 0;
  DDRD |= (1 << L1);
  /*TCCR2A = 0;
  TCCR2B = 0b00000010;
  TIMSK2 = 0b00000001;
  sei();
  */
}
 
void loop () {
  print_sos();
  stop_sos();
}

