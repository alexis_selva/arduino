Devoir de programmation semaine 1

Bien que nous ne sachions encore presque rien sur les microcontrôleurs, ce premier "devoir de programmation" sera l'occasion de tester ce type d'évaluations. Pour cette semaine, un seul exercice vous est proposé. Le résultat ne vaudra seulement 8 petits points pour la note finale, mais vous encourageons à le tester dès maintenant. Le but est de vous familiariser avec cette technique d'évaluation ! 

Durant les prochaines semaines, vous aurez des programmes à écrire. Cette semaine, un simple "copier-coller" depuis un exemple pris dans les programmes Arduino ou Energia va suffire. Le mécanisme de ces devoirs de programmation est décrit dans le document : correcteur.pdf . Une version en anglais : correcteur-en.pdf

Une vidéo de la semaine montre aussi comment procéder.

La donnée du devoir de cette semaine est de faire clignoter la LED à une fréquence de 0.5 Hz (donc une période de 2 secondes, soit un changement d'état chaque seconde). Comme vous n'êtes pas encore censé savoir programmer un microcontrôleur, il vous suffira d'aller dans le programme Arduino ou le programme Energia, de demander l'exemple "Blink" (menu : File - Examples - Basics - Blink). C'est ce programme que vous devrez donner au moment du "Submit".

Sur l'Arduino, c'est bien la pin 13 qui doit être utilisée, comme dans l'exemple standard Blink, et non la LED L1 du LearnCBot ! Sur le MSP430, c'est la LED rouge, qu'on trouve aussi bien sur le Launchpad que sur le LearnCbot.

NB : bien que le système afficheur une note qui est le total des devoirs Arduino et MSP430, la note finale sera calculée en prenant la meilleure note des deux, pour chaque semaine.
