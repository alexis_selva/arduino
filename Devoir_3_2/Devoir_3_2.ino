// Devoir 3 : 

#define   L1                    4
#define   PERIOD_DOT            200
#define   PERIOD_DASH           600
#define   PERIOD_BETWEEN_SIGN   200
#define   PERIOD_BETWEEN_CHAR   600
#define   PERIOD_BETWEEN_WORD   1400
#define   NB_CHAR               3

void led_on(byte led) {
  PORTD |= 1 << led;
}

void led_off(byte led) {
  PORTD &= ~(1 << led);
}

void print_sign(int period){
  led_on(L1);
  delay(period);
  led_off(L1);
}

void print_char(int period){
  for (int i = 0 ; i < NB_CHAR ; i++){
    print_sign(period);
    delay(PERIOD_BETWEEN_SIGN);
  }
}

void sos(void){
  print_char(PERIOD_DASH);
  delay(PERIOD_BETWEEN_CHAR);
  print_char(PERIOD_DOT);
  delay(PERIOD_BETWEEN_CHAR);
  print_char(PERIOD_DASH);
  delay(PERIOD_BETWEEN_WORD);
}
  
void setup () { 
  PIND = 0;
  PORTD = 0;
  DDRD |= (1 << L1);
} 
  
void loop () {
  sos();
}

