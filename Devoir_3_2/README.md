Devoir de programmation semaine 3

SOS du Titanic !

Le programme doit produire un signal de détresse lumineux en code Morse, sur la Led L1. Voici la succession des traits et des point pour former "S O S" : - - - . . . - - - 
Le message doit être répété toutes les 2 secondes.

Voici les contraintes temporelles : - durée d'un point (dot) : 0.2s - durée d'un trait (dash) : 0.6s - espace entre traits ou points : 0.2s - espace entre lettres : 0.6s - espace entre mots : 1.4s

Nous vous suggérons d'utiliser les fonctions (procédures) pour rendre lisible votre programme !

C'est seulement après plusieurs jours que quelqu'un a remarqué que les codes des lettre S et O ont été inversés...
Pour ne pas changer la donnée, nous vous proposons... de continuer à envoyer "OSO" !
