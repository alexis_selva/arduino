Devoir de programmation semaine 4

Commande d'un moteur pas-à-pas

Le principe du moteur pas-à-pas sera donné dans une vidéo de la semaine 7. Le dessin ci-dessous donne déjà une explication simplifiée :

Les enroulement A et B vont chacun produire un champ magnétique dans leur noyau qui peut être Nord-Sud ou Sud-Nord selon le sens du courant.
En imaginant que la partie mobile, représenté par un cercle, est un aimant permanent qui a aussi un pôle Nord et un pôle Sud, on comprend bien que cette partie mobile va changer de position selon le sens du courant dans chaque enroulement.

Le diagramme des temps montre la succession des commandes de chaque enroulement nécessaire pour faire tourner le moteur dans un sens ou dans l'autre. On remarque que le système a 4 états.

Regardez la vidéo de la séquence : pas-a-pas.mp4

On vous demande d'écrire un programme qui fait tourner le moteur dans le sens horaire lorsqu'il reçoit des impulsions sur le poussoir P1. De même, il fera tourner le moteur dans l'autre sens en recevant des impulsions du poussoir P2.

Le sens positif a la séquence suivante : L1=0, L2=0 -> L1=1, L2=0 ->L1=1, L2=1 ->L1=0, L2=1
