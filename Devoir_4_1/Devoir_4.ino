// Devoir 4 : 

#define   P1              2
#define   P2              3
#define   L1              4
#define   L2              5
#define   PERIOD          20

enum diretion_e { NORTH_WEST, SOUTH_WEST, SOUTH_EAST, NORTH_EAST };

static byte state = SOUTH_WEST;

static void led_on(byte led) {
  PORTD |= (1 << led);
}

static void led_off(byte led) {
  PORTD &= ~(1 << led);
}

static byte push_on(byte push) {
  return !(PIND & (1 << push));
}

static void change_leds(void) {
  switch (state) {
    case NORTH_WEST:
      led_on(L1);
      led_off(L2);
      break;
    case SOUTH_WEST:
      led_off(L1);
      led_off(L2);
      break;
    case SOUTH_EAST:
      led_off(L1);
      led_on(L2);
      break;
    case NORTH_EAST:
      led_on(L1);
      led_on(L2);
      break;
  }
}

static void change_state_P1(void) {
  static byte old_push = push_on(P1);
  byte new_push = push_on(P1);
  if ((!old_push) && (new_push)) {
    switch (state) {
      case NORTH_WEST:
        state = NORTH_EAST;
      break;
      case SOUTH_WEST:
        state = NORTH_WEST;
        break;
      case SOUTH_EAST:
        state = SOUTH_WEST;
        break;
      case NORTH_EAST:
        state = SOUTH_EAST;
        break;
    }
  }
  old_push = new_push;
}

static void change_state_P2(void) {
  static byte old_push = push_on(P2);
  byte new_push = push_on(P2);
  if ((!old_push) && (new_push)) {
    switch (state) {
      case NORTH_WEST:
        state = SOUTH_WEST;
      break;
      case SOUTH_WEST:
        state = SOUTH_EAST;
        break;
      case SOUTH_EAST:
        state = NORTH_EAST;
        break;
      case NORTH_EAST:
        state = NORTH_WEST;
        break;
    }
  }
  old_push = new_push;
}

void setup () {
  PIND = 0;
  PORTD = 0;
  DDRD |= (1 << L1) | (1 << L2);
}

void loop () {
  change_state_P1();
  change_state_P2();
  change_leds();
  delay(PERIOD);
}
