Devoir de programmation semaine 4

Serrure codée

Pour allumer la Led L1, il faut successivement : 
- appuyer sur P1 
- relâcher P1 
- appuyer à nouveau sur P1 
- appuyer sur P2 (sans relâcher P1) 
- relâcher P2 
- relâcher P1. C'est alors que L1 doit s'allumer durant trois seconde.

La vitesse d'exécution de la séquence n'est pas importante. Toute autre manipulation est considérée comme fausse. Elle doit être suivie : 
- de l'attente jusqu'à ce que plus aucune touche ne soit pressée 
- d'une attente supplémentaire d'une seconde. 
Ensuite, le programme doit reprendre sa détection.

Si vous avez le moindre doute, regardez la vidéo de la séquence correcte : téléchargez le lien Serrure.mp4 et regardez-le avec un lecteur comme VLC
