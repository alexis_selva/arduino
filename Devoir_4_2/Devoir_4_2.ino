// Devoir 4 : 

#define   P1              2
#define   P2              3
#define   L1              4
#define   L2              5
#define   PERIOD          200
#define   PERIOD_SUCCESS  3000
#define   PERIOD_ERROR    1000

enum push { START = 0, PUSH1_0N, PUSH1_OFF, PUSH1_ON_AGAIN, PUSH2_ON, PUSH2_OFF, PUSH1_OFF_AGAIN, END, ERROR };

static byte state = START;

static void led_on(byte led) {
  PORTD |= (1 << led);
}

static void led_off(byte led) {
  PORTD &= ~(1 << led);
}

static byte push_on(byte push) {
  return !(PIND & (1 << push));
}

static void success(void) {
  if (state == END) {
    led_on(L1);
    delay(PERIOD_SUCCESS);
    led_off(L1);
    state = START;
  }
}

static void error(void) {
  if (state == ERROR) {
    if ((push_on(P1)) || (push_on(P2))) {
      while ((!push_on(P1)) && (!push_on(P2))) { }
    }
    led_on(L2);
    delay (PERIOD_ERROR);
    led_off(L2);
    state = START;
  }
}

static void change_state_P1() {
  static byte old_push = push_on(P1);
  byte new_push = push_on(P1);
  if (old_push != new_push) {
    switch (state) {
      case START:
        if (new_push) {
          state = PUSH1_0N;
        } else {
          state = ERROR;
        }
        break;
      case PUSH1_0N:
        if ((old_push) && (!new_push)) {
          state = PUSH1_OFF;
        } else {
          state = ERROR;
        }
        break;
      case PUSH1_OFF:
        if (new_push) {
          state = PUSH1_ON_AGAIN;
        } else {
          state = ERROR;
        }
        break;
      case PUSH2_OFF:
        if (!new_push) {
          state = END;
        } else {
          state = ERROR;
        }
        break;
      default:
        state = ERROR;
        break;  
    }
  }
  old_push = new_push;
}

static void change_state_P2(void) {
  static byte old_push = push_on(P2);
  byte new_push = push_on(P2);
  if (old_push != new_push) {
    switch (state) {
      case PUSH1_ON_AGAIN:
        if (new_push) {
          state = PUSH2_ON;
        } else {
          state = ERROR;
        }
        break;
      case PUSH2_ON:
        if (!new_push) {
          state = PUSH2_OFF;
        } else {
          state = ERROR;
        }
        break;
      default:
        state = ERROR;
        break;
    }
  }
  old_push = new_push;
}

void setup () {
  PIND = 0;
  PORTD = 0;
  DDRD |= (1 << L1);
}

void loop () {
  change_state_P1();
  change_state_P2();
  success();
  error();
  delay(PERIOD);
}
