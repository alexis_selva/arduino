// Devoir 5_2 : sos 

#define   L1                      4
#define   PERIOD_BIT              30
#define   PERIOD_WAIT             303
#define   MAX_NUMBER_BITS         (8 + 3)
#define   MAX_NUMBER_BITS_SOS     (MAX_NUMBER_BITS * 3)

static volatile unsigned long wait = 0;
static byte index;
static byte buffer[MAX_NUMBER_BITS_SOS];

void led_on(byte led) {
  PORTD |= 1 << led;
}

void led_off(byte led) {
  PORTD &= ~(1 << led);
}

void print_bit(byte value){
  if (value) {
    led_on(L1);
  } else {     
    led_off(L1);
  }
}

void setup_character(char value) {
  buffer[index++] = 0;
  for (byte i = 0 ; i < 8 ; i++) {
    if (value & 1) {
      buffer[index++] = 0;
    } else {
      buffer[index++] = 1;
    }
    value >>= 1;
  }
  buffer[index++] = 1;
  buffer[index++] = 1;
}

void setup_sos(void) {
  setup_character('S');
  setup_character('O');
  setup_character('S');
}

ISR(TIMER2_OVF_vect) {
  static volatile unsigned int cnt = 0;
  TCNT2 = 56;
  if (cnt++ > PERIOD_BIT) {
    cnt = 0;
    if (wait) {
      print_bit(1);
      if (wait++ >= PERIOD_WAIT) {
        wait= 0;
      }
    } else {
      print_bit(buffer[index++]);
      if (index == MAX_NUMBER_BITS_SOS) {
        index = 0;
        wait = 1;
      }
    } 
  } 
}
 
void setup () { 
  setup_sos();
  index = 0;
  PIND = 0;
  PORTD = 0;
  DDRD |= (1 << L1);
  TCCR2A = 0;
  TCCR2B = 0b00000010;
  TIMSK2 = 0b00000001;
  sei();
  print_bit(1);
}
 
void loop () {
 
}
