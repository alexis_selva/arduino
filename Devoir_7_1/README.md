Devoir de programmation semaine 7

Commande d'un robot mobile : éviter les obstacles

Un robot mobile à deux roues motrices (et un point d'appui) doit éviter les obstacles grâce à ses moustaches, qui sont deux interrupteurs qui détectent les contact à l'avant du robot, l'un à droite, l'autre à gauche.

On vous demande de programmer son déplacement selon l’algorithme suivant : 
- tant qu'il ne voit pas d'obstacles, le robot avance (en faisant tourner ses deux roues dans le sens "avance") 
- s'il détecte un obstacle avec une de ses moustache, il doit reculer pendant une seconde (en faisant tourner les deux roues dans le sens "recule"), puis pivoter sur lui-même pendant une demi-seconde (en activant une roue dans un sens et l'autre roue dans l'autre sens : prenez garde au sens du pivotement) 
- il reprend ensuite sa marche en avant.

Chaque moteur (droite et gauche) dispose de deux signaux de commande : Avance et Recule. 
Le système a donc 2 entrées actives à "0": 
- moustache droite (P1) 
- moustache gauche (P2) 
et 4 sorties actives à "1": 
- Avance droite (L1) 
- Recule droite (L2) 
- Avance gauche (L3) 
- Recule gauche (L4)


Le module LearnCbot convient parfaitement pour l'expérimentation ! 
