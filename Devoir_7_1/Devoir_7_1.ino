/* ASSIGNEMENT 7.1 */

/* DEFINITIONS */

#define PUSH_1 2
#define PUSH_2 3

#define LED_1  4
#define LED_2  5
#define LED_3  6
#define LED_4  7

#define MOUSTACHE_DROITE  PUSH_1
#define MOUSTACHE_GAUCHE  PUSH_2 

#define MOTEUR_DROIT_AVANCE  LED_1
#define MOTEUR_DROIT_RECULE  LED_2
#define MOTEUR_GAUCHE_AVANCE LED_3
#define MOTEUR_GAUCHE_RECULE LED_4

#define MAX_PFM 16

#define VITESSE_AVANCE     MAX_PFM
#define VITESSE_EVITE      MAX_PFM

#define COMPTEUR_PFM 20
#define COMPTEUR_MOUSTACHE 200

#define DUREE_RECULE  1000
#define DUREE_PIVOTE  500

#define TIMER_CORRECTION 12

enum {
  OFFSET_MOUSTACHE_DROITE,
  OFFSET_MOUSTACHE_GAUCHE,
};

/* VARIABLES */

static char vitD;
static char vitG;
static volatile char pfmD;
static volatile char pfmG;
static volatile byte vstable;
static byte etat_moustache;

/* FUNCTIONS */

static inline void moteur_droit_avance(void) {
  bitClear (PORTD, MOTEUR_DROIT_RECULE); 
  bitSet   (PORTD, MOTEUR_DROIT_AVANCE);
}

static inline void moteur_droit_recule(void) {
  bitSet   (PORTD, MOTEUR_DROIT_RECULE); 
  bitClear (PORTD, MOTEUR_DROIT_AVANCE);
}

static inline void moteur_droit_stoppe(void) {
  bitClear (PORTD,MOTEUR_DROIT_RECULE); 
  bitClear (PORTD,MOTEUR_DROIT_AVANCE);
}

static inline void moteur_gauche_avance(void) {
  bitClear (PORTD,MOTEUR_GAUCHE_RECULE);
  bitSet   (PORTD,MOTEUR_GAUCHE_AVANCE);
}

static inline void moteur_gauche_recule(void) {
  bitSet   (PORTD,MOTEUR_GAUCHE_RECULE); 
  bitClear (PORTD,MOTEUR_GAUCHE_AVANCE);
}

static inline void moteur_gauche_stoppe(void) {
  bitClear (PORTD,MOTEUR_GAUCHE_RECULE); 
  bitClear (PORTD,MOTEUR_GAUCHE_AVANCE);
}

static inline void setup_in_out (void) { 
  PIND = 0;
  PORTD = 0;
  DDRD = 0;
  bitSet (DDRD, MOTEUR_GAUCHE_RECULE); 
  bitSet (DDRD, MOTEUR_GAUCHE_AVANCE);
  bitSet (DDRD, MOTEUR_DROIT_RECULE); 
  bitSet (DDRD, MOTEUR_DROIT_AVANCE);  
}

static inline void do_pfm (char pfd, char pfg) {
  static volatile byte pfmCntd;
  static volatile byte pfmCntg;
  
  if (pfd >= 0) {
    if (pfd > MAX_PFM) {
      pfd = MAX_PFM;
    }
    pfmCntd += pfd;
    if (pfmCntd > MAX_PFM - 1) {
      pfmCntd &= (MAX_PFM - 1); 
      moteur_droit_avance();
    } else { 
      //moteur_droit_stoppe(); 
    }
  } else {
    pfd= -pfd;
    if (pfd > MAX_PFM) {
      pfd = MAX_PFM;
    } 
    pfmCntd += pfd;
    if (pfmCntd > MAX_PFM - 1) {
      pfmCntd &= (MAX_PFM - 1); 
      moteur_droit_recule();
    } else { 
      //moteur_droit_stoppe();
    }
  }
   
  if (pfg >= 0) {
    if (pfg > MAX_PFM) { 
      pfg = MAX_PFM;
    }
    pfmCntg += pfg;
    if (pfmCntg > MAX_PFM - 1) {
      pfmCntg &= (MAX_PFM - 1); 
      moteur_gauche_avance();
    } else { 
      //moteur_gauche_stoppe(); 
    }
  } else {
    pfg= -pfg;
    if (pfg > MAX_PFM) {
      pfg = MAX_PFM;
    }
    pfmCntg += pfg;
    if (pfmCntg > MAX_PFM - 1) {
      pfmCntg &= (MAX_PFM - 1);
      moteur_gauche_recule();
    } else { 
      //moteur_gauche_stoppe(); 
    }
  }
}

static inline void set_vitesse (char vd, char vg) {  
  if (vd > pfmD) {
    pfmD++; 
    vstable = 0;
  }
  if (vd < pfmD) {
    pfmD--; 
    vstable = 0;
   }
  if (vg > pfmG) {
    pfmG++; 
    vstable = 0;
  }
  if (vg < pfmG) {
    pfmG--; 
    vstable = 0;
  }
  if ((vitD == pfmD) && (vitG == pfmG)) {
    vstable = 1;
  }
}

static inline void next_vitesse (char vd, char vg) {
  vitD = vd; vitG = vg;
  delay (10);
  while (!vstable) {}
  delayMicroseconds (10);
}

static inline void get_etat_moustache (void)  {
  static volatile byte old_etat;
  byte etat = PIND & (1 << MOUSTACHE_DROITE | 1<< MOUSTACHE_GAUCHE); 
  if (etat ^ old_etat) {
    if (!(etat & 1 << MOUSTACHE_DROITE)) {
      bitSet (etat_moustache, OFFSET_MOUSTACHE_DROITE) ;
    }
    if (!(etat & 1 << MOUSTACHE_GAUCHE)) {
      bitSet (etat_moustache, OFFSET_MOUSTACHE_GAUCHE) ;
    }
  }
  old_etat = etat;
}

ISR (TIMER2_OVF_vect) {
  static volatile byte cnt1;
  static volatile unsigned int cnt2;
  
  /* Rearm the timer every 100us (200 * 8 * 62.5us)
     considering a correction of few us */
  TCNT2 = 256 - 200 + TIMER_CORRECTION;
  
  if (cnt1++ > COMPTEUR_PFM) {
    cnt1 = 0; 
    if ((vitD == pfmD) && (vitG == pfmG)) {
      vstable = 1;
    } else {
      vstable = 0;
    }
    do_pfm (pfmD, pfmG);
  }
  
  if (cnt2++ > COMPTEUR_MOUSTACHE) {
    cnt2 = 0; 
    set_vitesse (vitD, vitG);
    get_etat_moustache ();
  }
}

/* Setup the interruption for the TIMER2 */
static inline void setup_timer(void) {
  /*
  cli();
  TCCR2A = 0;
  TCCR2B = 0b00000010; 
  TIMSK2 = 0b00000001;
  sei();
  */
}

void setup()  { 
  setup_in_out();  
  setup_timer();
  /*
  vitD = VITESSE_AVANCE; 
  vitG = VITESSE_AVANCE;
  pfmD = VITESSE_AVANCE;
  pfmG = VITESSE_AVANCE;
  */
  //next_vitesse (VITESSE_AVANCE, VITESSE_AVANCE);
  moteur_gauche_avance();
  moteur_droit_avance();
}

byte push_on(byte push) {
  return !(PIND & (1 << push));
}

void loop () {
  // obstacle à droite ?
  //if (etat_moustache & (1 << OFFSET_MOUSTACHE_DROITE)) {
  if (push_on(MOUSTACHE_DROITE)) {
    //next_vitesse (-VITESSE_EVITE, -VITESSE_EVITE);
    moteur_gauche_recule();
    moteur_droit_recule();
    unsigned long start = millis();
    while (millis() - start < DUREE_RECULE){ }
    //next_vitesse (-VITESSE_EVITE, VITESSE_EVITE);
    moteur_gauche_recule();
    moteur_droit_avance();
    start = millis(); 
    while (millis() - start < DUREE_PIVOTE){ }
    //next_vitesse (VITESSE_AVANCE, VITESSE_AVANCE);
    bitClear (etat_moustache, OFFSET_MOUSTACHE_DROITE);
    moteur_gauche_avance();
    moteur_droit_avance();
  }
  
  // obstacle à gauche ?
  //if (etat_moustache & (1 << OFFSET_MOUSTACHE_GAUCHE)) {
  if (push_on(MOUSTACHE_GAUCHE)) {
    //next_vitesse (-VITESSE_EVITE, -VITESSE_EVITE);
    moteur_gauche_recule();
    moteur_droit_recule();
    unsigned long start = millis();
    while (millis() - start < DUREE_RECULE){ }
    //next_vitesse (VITESSE_EVITE, -VITESSE_EVITE);
    moteur_gauche_avance();
    moteur_droit_recule();
    start = millis();
    while (millis() - start < DUREE_PIVOTE){ }
    //next_vitesse (VITESSE_AVANCE, VITESSE_AVANCE);
    bitClear (etat_moustache, OFFSET_MOUSTACHE_GAUCHE);
    moteur_gauche_avance();
    moteur_droit_avance();
  }  
}




