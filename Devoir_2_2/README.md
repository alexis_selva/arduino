Devoir de programmation semaine 2

Feu à l'entrée d'un parking

Le propriétaire d'un parking pour voitures souhaite ajouter un système indiquant si le parking est plein. 
Deux barrières lumineuses sont placées à l'entrée et à la sortie. Un feu rouge montre aux visiteurs s'ils doivent attendre.

L'entrée Pous1 sera utilisée pour la barrière placée à l'entrée de la salle. 
Pous2 sera utilisé pour la barrière de sortie (qui est une entrée pour le microcontrôleur!). 
Led1 sera utilisée comme feu rouge. 
Pour le surveillant du parking, un témoin s'allumera quand le parking est considéré comme vide (Led2). 
Pour faciliter le test, le nombre maximum de voitures est fixé à 5.

La suppression des rebonds de contact n'est pas en soi un problème : les barrières fournissent des signaux "propres". Mais il vous suffira d'ajouter une attende de 20 ms dans la boucle principale pour que vos tests avec des poussoirs fonctionnent correctement.

