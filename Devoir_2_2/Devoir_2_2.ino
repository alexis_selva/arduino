// Devoir 2 : 

#define   L1            4
#define   L2            5
#define   LED_1_ON      digitalWrite (L1,1)
#define   LED_1_OFF     digitalWrite (L1,0)
#define   LED_2_ON      digitalWrite (L2,1)
#define   LED_2_OFF     digitalWrite (L2,0)
#define   PERIODE       20
#define   WAIT          delay(PERIODE)
#define   P1            2
#define   P2            3
#define   POUSSOIR_1_ON (digitalRead (P1) == 0)
#define   POUSSOIR_2_ON (digitalRead (P2) == 0)
#define   NB_PLACES     5

void setup() {
  pinMode (P1, INPUT);
  pinMode (P2, INPUT);
  pinMode (L1, OUTPUT);
  pinMode (L2, OUTPUT);
}

char voiture = 0;
void loop () {
  if (voiture == 0) {
    LED_2_ON;
  } else if ((voiture > 0) && (voiture < NB_PLACES)) {
    LED_1_OFF;
    LED_2_OFF;
  } else {
    LED_1_ON;
  }
  if ((POUSSOIR_1_ON) && (voiture < NB_PLACES)) {
    while (POUSSOIR_1_ON) { };
    voiture++;
  }
  if ((POUSSOIR_2_ON) && (voiture > 0)) {
    while (POUSSOIR_2_ON) { };
    voiture--;
  }
  WAIT;
}

